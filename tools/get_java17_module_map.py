#!python3

from typing import Dict
from bs4 import BeautifulSoup

import requests

urlsToCheck = [
    "https://openjfx.io/javadoc/17/allpackages-index.html",
    "https://docs.oracle.com/en/java/javase/17/docs/api/allpackages-index.html"
]

module_map: Dict[str, str] = {}
for place in urlsToCheck:
    request = requests.get(place)

    if request.status_code == 200:
        # it worked
        soup = BeautifulSoup(request.text, "html.parser")
        all_package_a_tags = soup.find("main").find_all("a")

        for a in all_package_a_tags:
            link: str = a["href"]
            if not link.endswith("/package-summary.html"):
                continue

            stripped_link = link.replace("/package-summary.html", "")
            module, _, raw_package = stripped_link.partition("/")
            package = raw_package.replace("/", ".")

            module_map[package] = module

    else:
        print("Somethings borken, yo. Code " + str(request.status_code))

with open("java17_package_map.json", "w") as out:
    out.write("{\n")

    for i, (package, module) in enumerate(module_map.items()):
        out.write(f"    \"{package}\": \"{module}\"")

        if i != len(module_map) - 1:
            out.write(",")
        out.write("\n")

    out.write("}\n")
