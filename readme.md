# Javadoc Switcher ReSwitched

A userscript based on a userscript written by my mate Skardan.

It redirects navigation to JDK or JFX javadocs to the java17 version.

## Misc

### Original Scripts

The original script can be found in [Javadocs_Switcher.user.js.old](Javadocs_Switcher.user.js.old). My adapted version can be found
in [Javadocs_Switcher_ReSwitched.user.js.old](Javadocs_Switcher_ReSwitched.user.js.old). Both may or may not be functional, they are kept around mostly for sentimental value.

### get_java17_module_map.py

A script can be found in [tools/get_java17_module_map.py](tools/get_java17_module_map.py). This is used to generate [java17_package_map.json](java17_package_map.json) by grabbing the package index
for the JDK and JFX docs respectively, then extracting the package -> module mappings from the links present on these pages. The script
depends on BeautifulSoup4 and requests.py 
