// ==UserScript==
// @name         Javadocs Switcher ReSwitched xTreme
// @namespace    lul
// @version      2.0
// @description  Switches any javadoc page between 11 and 16 to Java 17. Originally by Skardan, now with added fuckery.
// @author       Lafreakshow
// @match        https://docs.oracle.com/*
// @match        https://openjfx.io/javadoc/*
// @icon         https://www.google.com/s2/favicons?domain=oracle.com
// @resource     module_map java17_package_map.json
// @grant        GM.getResourceUrl
// ==/UserScript==

(function () {
    'use strict'
    const TARGET_VERSION = "17"

    async function getPackageMap() {
        const source = GM.getResourceUrl("module_map")
        return source.then(result => {
            const sanitised = result.replace("data:application;base64,", "")
            return JSON.parse(atob(sanitised))
        })
    }

    /*
     * Note: A list of urls to test regex against:
     * https://docs.oracle.com/javase/8/javafx/api/javafx/application/Application.html
     * https://docs.oracle.com/en/java/javase/12/docs/api/java.base/java/lang/StringBuilder.html
     * https://docs.oracle.com/javase/7/docs/api/java/lang/StringBuilder.html
     * https://openjfx.io/javadoc/17/javafx.graphics/javafx/application/Application.html
     *
     */

    // Note: checking for "openjfx" because openjfx url is not guaranteed to contain the string"javafx"
    const IS_JFX_REGEX          = /openjfx|\/javafx\//
    const EXTRACT_VERSION_REGEX = /\/(\d{1,2})\//
    // If anyone tells you the non-capturing groups are unnecessary, DO NOT LISTEN.
    // The script needs some refactoring before that statement is true because right now,
    // the script depends on there being only one group present in the match result.
    const EXTRACT_LOCATION_REGEX = /(?:(?:\/\d{1,2}\/)(?:(?:javafx\/|docs\/)?api\/)*)(.*$)/

    function isJFXDoc(url) { return RegExp(IS_JFX_REGEX).test(url) }

    // I'm just gonna assume this actually matches. Should only be called on matching urls in the first place
    function extractVersion(url) { return url.match(EXTRACT_VERSION_REGEX)[1] }

    // I'm just gonna assume this actually matches. Should only be called on matching urls in the first place
    function extractLocation(url) { return url.match(EXTRACT_LOCATION_REGEX)[1] }

    async function determineNewURL() {
        const oldUrl  = window.location.href
        const version = extractVersion(oldUrl)

        if (version === TARGET_VERSION)
            return Promise.reject("Redirect unnecessary, already on target version")
        const isJFX     = isJFXDoc(oldUrl)
        let newLocation = extractLocation(oldUrl)

        if (version < 11) {
            const module_map  = await getPackageMap()
            const packageName = newLocation.substring(0, newLocation.lastIndexOf("/")).replace("/", ".")
            const module      = module_map[packageName]
            if (module !== undefined) newLocation = `${module}/${newLocation}`
        }

        let newURL
        if (isJFX) newURL = `https://openjfx.io/javadoc/${TARGET_VERSION}/${newLocation}`
        else newURL = `https://docs.oracle.com/en/java/javase/${TARGET_VERSION}/docs/api/${newLocation}`

        return Promise.resolve(newURL)
    }

    function main() {
        determineNewURL()
            .then(result => {
                console.log("Redirecting ->" + result)
                window.location.assign(result)
            })
            .catch(reason => console.log(reason))
    }

    main()
})()
